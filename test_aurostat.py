from flask import Flask, render_template
import psycopg2

app = Flask(__name__)
app.config.from_object(__name__)


def get_data():
	attribute_monthly_data = dict()
	attribute_total_data = dict()
	conn = psycopg2.connect("dbname='aurostat' user='aurostat' password='auroville' host='localhost'")
	cur = conn.cursor()
	attributes = ['persons', 'two_wheelers', 'cars', 'vans', 'buses', 'autos']
	for attribute in attributes:
		total_every_month = []
		for month in range(1, 13):
			cur.execute('select sum(%s) from aurostat.visitor_center where extract (month from date) = %d' % (attribute, month))
			item = cur.fetchall()[0][0]
			if item != None:
				total_every_month.append(item)
		attribute_monthly_data[attribute] = total_every_month
	for attribute in attributes:
		cur.execute('select sum(%s) from aurostat.visitor_center' % attribute)
		attribute_total_data[attribute] = cur.fetchall()[0][0]
	conn.close()
	return ( attribute_monthly_data, attribute_total_data)



@app.route('/', methods=['GET', 'POST'])
def index():
	return render_template('index.html', monthly_data = get_data()[0], total_data=get_data()[1])


if __name__ == '__main__':
	app.run(debug=True)
