$(document).ready(function () {
      $('#container').highcharts({
          title: {
              text: 'Data from the Visitor Center',
              x: -20 //center
          },
          xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
          },
          yAxis: {
              title: {
                text: 'Total numbers'
              },
              plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
              }]
           },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle',
              borderWidth: 0
          },
          credits: {
              enabled: false
          },
          series: [{
                  name: 'Persons',
                  data: {{ monthly_data['persons'] }}
          }, {
                  name: 'Two Wheelers',
                  data: {{ monthly_data['two_wheelers'] }}
          }, {
                  name: 'Cars',
                  data: {{ monthly_data['cars'] }}

          }, {
                  name: 'Vans',
                  data: {{ monthly_data['vans'] }}
          }, {
                  name: 'Buses',
                  data:  {{ monthly_data['buses'] }}
          },  {
                  name: 'Autos',
                  data:  {{ monthly_data['autos'] }}
          }]
      });
});
